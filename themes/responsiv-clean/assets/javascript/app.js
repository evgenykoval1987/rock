/*
 * Application
 */

$(document).tooltip({
    selector: "[data-toggle=tooltip]"
})

function toggleSidebar() {
    $('#layout-sidebar').toggleClass('in')
}

$(document).on('click','.like',function(){
	var operation = ($(this).hasClass('active')) ? 'unlike' : 'like';

	if (operation == 'like'){
		$(this).find('span').text(parseInt($(this).find('span').text()) + 1);
		$(this).addClass('active');
	}
	if (operation == 'unlike'){
		$(this).find('span').text(parseInt($(this).find('span').text()) - 1);
		$(this).removeClass('active');
		$(this).blur()
	}

	var post_id = $(this).data('post-id');
	$(this).request('onLike', {
		url: '/',
		data: {
			operation: operation,
			post_id: post_id
		}
	}); 
	return false;
});


function vk_popup(options){
	var screenX = typeof window.screenX != 'undefined' ? window.screenX : window.screenLeft,
		screenY = typeof window.screenY != 'undefined' ? window.screenY : window.screenTop,
		outerWidth = typeof window.outerWidth != 'undefined' ? window.outerWidth : document.body.clientWidth,
		outerHeight = typeof window.outerHeight != 'undefined' ? window.outerHeight : (document.body.clientHeight - 22),
		width = options.width,
		height = options.height,
		left = parseInt(screenX + ((outerWidth - width) / 2), 10),
		top = parseInt(screenY + ((outerHeight - height) / 2.5), 10),
		features = ('width=' + width + ',height=' + height + ',left=' + left + ',top=' + top);
		return window.open(options.url, 'vk_oauth', features);
}
function loginVk(redirect_uri) {
	var win;
	var uri_regex = new RegExp('kaktv.top');
	var url = 'http://oauth.vkontakte.ru/authorize?client_id=7209627&display=popup&scope=email&redirect_uri=' + redirect_uri;
	win = vk_popup({
		width:620,
		height:370,
		url:url
	});
	var watch_timer = setInterval(function () {
		try {
			if (uri_regex.test(win.location)) {
				clearInterval(watch_timer);
				setTimeout(function () {
					win.close();
					document.location.reload();
				}, 500);
			}
		} catch (e) {}
	}, 100);
}

window.fbAsyncInit = function() { 
    FB.init({
      	appId      : '557995528282806',//Replace this App ID with Yours
      	cookie     : true,  // enable cookies to allow the server to access 
      	status     : true,                    // the session
      	xfbml      : true,  // parse social plugins on this page
      	oauth      : true,
      	version    : 'v2.8' // use version 2.2
    });
    FB.AppEvents.logPageView();

    function updateButton(response) {  
      	console.log(response);
      	button       =   document.getElementById('fb-auth');//document.getElementById('fb-auth');
      	userInfo     =   document.getElementById('user-info');
      	if (response.authResponse) {
          	button.onclick = function() {
              	FB.logout(function(response) {
                  	FB.login(function(response) {
                      	if (response.authResponse) {
                          	FB.api('/me', function(info) {
                              	login(response, info);
                          	});
                      	} 
                  	}, {
                      	scope:'email'
                  	});
              	});

              	return false;
          	};
      	} else {
          	button.onclick = function() {
             	FB.login(function(response) {
                  	if (response.authResponse) {
                      	FB.api('/me', function(info) {
                          	login(response, info);
                      	});
                  	}
              	}, {
                  	scope:'email'
              	});

              	return false;
          	}
      	}
  	}
  	
  	FB.getLoginStatus(updateButton);
  	FB.Event.subscribe('auth.statusChange', updateButton);
}


var e = document.createElement('script');
e.async = true;
e.src = /*document.location.protocol+*/'//connect.facebook.net/en_US/sdk.js'; 
document.getElementById('fb-root').appendChild(e);
 
function showLoader(status){
    if (status){
        $('#loading-image').show();
    }
    else
        $('#loading-image').hide();
}
function login(response, info){ 
    if (response.authResponse) {
        showLoader(false);
        fqlQuery();
    }
}
function fqlQuery(){
    FB.api('/me', {fields: "id,picture,email,first_name,last_name"}, function(response) {
        console.log(response);
        $.ajax({
            url: 'https://kaktv.top/social/fb',
            data : {
            	'email' : response.email,
            	'first_name' : response.first_name,
            	'last_name' : response.last_name,
            	'image' : response.picture.data.url
            },
            type : 'post',
            success: function(result) {
                if(result == '1'){
                    window.location.reload();
                } else if(result == 'registered'){
                    window.location.reload();
                } else{
                    $('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>Error: Please try again later or login with password</div>').insertBefore($('div.account-login'));
                }
                
            }
        });
    });
}

var googleUser = {};
var startApp = function() {
    gapi.load('auth2', function(){
      	auth2 = gapi.auth2.init({
        	client_id: '1006997711414-jphilu94c1mkg6dt4rdiinm5ftun56bi.apps.googleusercontent.com',
        	cookiepolicy: 'profile',
      	}); 
      	attachSignin(document.getElementById('g-auth'));
      	return false;
    });
};

function attachSignin(element) {
    auth2.attachClickHandler(element, {},
        function(googleUser) {
          	var firstname = googleUser.getBasicProfile().getGivenName();
          	var lastname = googleUser.getBasicProfile().getFamilyName();
          	var email = googleUser.getBasicProfile().getEmail();
          	var photo = googleUser.getBasicProfile().getImageUrl();
          	
          	$.ajax({
            	url: 'https://kaktv.top/social/gp',
            	data : 'email='+email+'&first_name='+firstname+'&last_name='+lastname+'&login_via=google'+'&photo='+photo,
            	type : 'post',
            	success: function(result) { 
	                if(result == '1'){
	                    window.location.reload();
	                } else if(result == 'registered'){
	                    window.location.reload();
	                } else{
	                    $('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>Error: Please try again later or login with password</div>').insertBefore($('div.account-login'));
	                }
            	}
          	});
          //alert(googleUser.getBasicProfile().getGivenName());
        }, function(error) {
          //alert(JSON.stringify(error, undefined, 2));
    });
}
startApp(); 

function mailru_popup(options){
	var screenX = typeof window.screenX != 'undefined' ? window.screenX : window.screenLeft,
		screenY = typeof window.screenY != 'undefined' ? window.screenY : window.screenTop,
		outerWidth = typeof window.outerWidth != 'undefined' ? window.outerWidth : document.body.clientWidth,
		outerHeight = typeof window.outerHeight != 'undefined' ? window.outerHeight : (document.body.clientHeight - 22),
		width = options.width,
		height = options.height,
		left = parseInt(screenX + ((outerWidth - width) / 2), 10),
		top = parseInt(screenY + ((outerHeight - height) / 2.5), 10),
		features = ('width=' + width + ',height=' + height + ',left=' + left + ',top=' + top);
		return window.open(options.url, 'vk_oauth', features);
}
function loginMailru(redirect_uri) {
	var win;
	var uri_regex = new RegExp('kaktv.top');
	var url = 'https://connect.mail.ru/oauth/authorize?client_id=768132&response_type=code&redirect_uri=' + redirect_uri;
	win = mailru_popup({
		width:620,
		height:370,
		url:url
	});
	var watch_timer = setInterval(function () {
		try {
			if (uri_regex.test(win.location)) {
				clearInterval(watch_timer);
				setTimeout(function () {
					win.close();
					document.location.reload();
				}, 500);
			}
		} catch (e) {}
	}, 100);
}

