<?php 
class Cms5dfd21427e388578568159_792e4301a69178c809ef7b2fb22c65a0Class extends Cms\Classes\PageCode
{
public function onPagePosts()
{   
    $this->blogTagSearch->setProperty('pageNumber', post('page'));
    $this->pageCycle();
}
public function onStart()
{
    $this->addCss('assets/ktv/category/css/styles.css');
    $this->addCss('assets/ktv/category/css/responsive.css');
    
}
public function onEnd(){ 
	if ($this->blogTagSearch) {
		if($this->blogTagSearch->tag){ 
		    $this->page->meta_title = $this->blogTagSearch->tag->name;
		    $this->page->meta_description = $this->blogTagSearch->tag->name;
		}
	}
}
}
