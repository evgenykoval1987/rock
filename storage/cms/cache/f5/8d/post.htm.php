<?php 
class Cms5dfd2139a6040170784078_d69fdbc1f42ca5221cdb0757db14d1e3Class extends Cms\Classes\PageCode
{
public function onEnd()
{
    if ($this->post) {
        $this->page->meta_title = $this->post->meta_title;
        $this->page->meta_description = $this->post->meta_description;
        if ($this->post->meta_title == '')
            $this->page->meta_title = $this->post->title;
        if ($this->post->meta_description == '')
            $this->page->meta_title = $this->post->title;
    }
    else {
        return Redirect::to($this->pageUrl('404'));
    }
}
public function onStart()
{
    $this->addCss('assets/ktv/single-post-1/css/styles.css');
    $this->addCss('assets/ktv/single-post-1/css/responsive.css');
}

public function onPageComments()
{ 
    $this->commentsPost->setProperty('pageNumber', post('page'));
    $this->pageCycle();
}
}
