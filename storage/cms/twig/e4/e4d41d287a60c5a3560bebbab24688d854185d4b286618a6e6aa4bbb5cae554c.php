<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/themes/responsiv-clean/partials/site/footer.htm */
class __TwigTemplate_65e1aad5fabd10f592cc1d994fcac08b59676e58c6aa56e438e3e610d8e5846a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<footer>
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t\t<div class=\"footer-section\">
\t\t\t\t\t<a class=\"logo\" href=\"";
        // line 6
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "\"><img src=\"";
        echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 6), "logo", [], "any", false, false, false, 6));
        echo "\" alt=\"Logo Image\"></a>
\t\t\t\t\t<ul class=\"icons\">
\t\t\t\t\t\t";
        // line 8
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 8), "facebook_url", [], "any", false, false, false, 8)) {
            // line 9
            echo "\t\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 9), "facebook_url", [], "any", false, false, false, 9), "html", null, true);
            echo "\" target=\"_blank\"><span class=\"iconify\" data-icon=\"ion-logo-facebook\" data-inline=\"false\"></span></a></li>
\t\t\t\t\t\t";
        }
        // line 11
        echo "\t\t\t\t\t\t";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 11), "twitter_url", [], "any", false, false, false, 11)) {
            // line 12
            echo "\t\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 12), "twitter_url", [], "any", false, false, false, 12), "html", null, true);
            echo "\" target=\"_blank\"><span class=\"iconify\" data-icon=\"ion-logo-twitter\" data-inline=\"false\"></span></a></li>
\t\t\t\t\t\t";
        }
        // line 14
        echo "\t\t\t\t\t\t";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 14), "instagram_url", [], "any", false, false, false, 14)) {
            // line 15
            echo "\t\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 15), "instagram_url", [], "any", false, false, false, 15), "html", null, true);
            echo "\" target=\"_blank\"><span class=\"iconify\" data-icon=\"ion-logo-instagram\" data-inline=\"false\"></span></a></li>
\t\t\t\t\t\t";
        }
        // line 17
        echo "\t\t\t\t\t\t";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 17), "vk_url", [], "any", false, false, false, 17)) {
            // line 18
            echo "\t\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 18), "vk_url", [], "any", false, false, false, 18), "html", null, true);
            echo "\" target=\"_blank\"><span class=\"iconify\" data-icon=\"ion-logo-vk\" data-inline=\"false\"></span></a></li>
\t\t\t\t\t\t";
        }
        // line 20
        echo "\t\t\t\t\t\t";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 20), "pinterest_url", [], "any", false, false, false, 20)) {
            // line 21
            echo "\t\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 21), "pinterest_url", [], "any", false, false, false, 21), "html", null, true);
            echo "\" target=\"_blank\"><span class=\"iconify\" data-icon=\"ion-logo-pinterest\" data-inline=\"false\"></span></a></li>
\t\t\t\t\t\t";
        }
        // line 23
        echo "\t\t\t\t\t\t";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 23), "mailru_url", [], "any", false, false, false, 23)) {
            // line 24
            echo "\t\t\t\t\t\t\t<li><a href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 24), "mailru_url", [], "any", false, false, false, 24), "html", null, true);
            echo "\" target=\"_blank\"><span class=\"iconify\" data-icon=\"ion-at-circle-outline\" data-inline=\"false\"></span></a>
\t\t\t\t\t\t";
        }
        // line 26
        echo "\t\t\t\t\t\t
\t\t\t\t\t</ul>

\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t\t\t<div class=\"footer-section\">
\t\t\t\t\t<h4 class=\"title\"><b>Категории</b></h4>
\t\t\t\t\t
\t\t\t\t\t";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_array_batch(($context["categories"] ?? null), 3));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 36
            echo "\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t";
            // line 37
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["category"]);
            foreach ($context['_seq'] as $context["_key"] => $context["column"]) {
                // line 38
                echo "\t\t\t\t\t\t\t\t<li><a href=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["column"], "url", [], "any", false, false, false, 38), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["column"], "name", [], "any", false, false, false, 38), "html", null, true);
                echo "</a></li>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['column'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 40
            echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "\t\t\t\t\t
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t\t<div class=\"footer-section\">
        \t\t\t<div id=\"fb-root\"></div>
\t\t\t        <div id=\"user-info\"></div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"login\">
\t\t\t  \t<div class=\"modal-dialog\" role=\"document\">
\t\t\t    \t<div class=\"modal-content\">
\t\t\t      \t\t<div class=\"modal-header\">
\t\t\t        \t\t<h5 class=\"modal-title\">Войти с помощью</h5>
\t\t\t        \t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
\t\t\t          \t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t\t        \t\t</button>
\t\t\t      \t\t</div>
\t\t\t      \t\t<div class=\"modal-body\">
\t\t\t        \t\t<p style=\"text-align: center;\">
\t\t\t        \t\t\t<a href=\"#\" onClick=\"loginVk('";
        // line 62
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("social", ["slug" => "vk"]);
        echo "'); return false;\"><img src=\"";
        echo $this->extensions['System\Twig\Extension']->mediaFilter("social/vk.png");
        echo "\"></a>&nbsp;
\t\t\t        \t\t\t<a href=\"#\" id=\"fb-auth\"><img src=\"";
        // line 63
        echo $this->extensions['System\Twig\Extension']->mediaFilter("social/fb.png");
        echo "\"></a>
\t\t\t        \t\t\t<a href=\"#\" id=\"g-auth\"><img src=\"";
        // line 64
        echo $this->extensions['System\Twig\Extension']->mediaFilter("social/gp.png");
        echo "\"></a>
\t\t\t        \t\t\t<a href=\"#\"  onClick=\"loginMailru('";
        // line 65
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("social", ["slug" => "mailru"]);
        echo "'); return false;\"><img src=\"";
        echo $this->extensions['System\Twig\Extension']->mediaFilter("social/mailru.png");
        echo "\"></a>
\t\t\t        \t\t</p>
\t\t\t      \t\t</div>
\t\t\t    \t</div>
\t\t\t  \t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</footer>";
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/site/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 65,  176 => 64,  172 => 63,  166 => 62,  144 => 42,  137 => 40,  126 => 38,  122 => 37,  119 => 36,  115 => 35,  104 => 26,  98 => 24,  95 => 23,  89 => 21,  86 => 20,  80 => 18,  77 => 17,  71 => 15,  68 => 14,  62 => 12,  59 => 11,  53 => 9,  51 => 8,  44 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<footer>
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t\t<div class=\"footer-section\">
\t\t\t\t\t<a class=\"logo\" href=\"{{ 'home'|page }}\"><img src=\"{{ this.theme.logo|media }}\" alt=\"Logo Image\"></a>
\t\t\t\t\t<ul class=\"icons\">
\t\t\t\t\t\t{% if this.theme.facebook_url %}
\t\t\t\t\t\t\t<li><a href=\"{{this.theme.facebook_url}}\" target=\"_blank\"><span class=\"iconify\" data-icon=\"ion-logo-facebook\" data-inline=\"false\"></span></a></li>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t{% if this.theme.twitter_url %}
\t\t\t\t\t\t\t<li><a href=\"{{this.theme.twitter_url}}\" target=\"_blank\"><span class=\"iconify\" data-icon=\"ion-logo-twitter\" data-inline=\"false\"></span></a></li>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t{% if this.theme.instagram_url %}
\t\t\t\t\t\t\t<li><a href=\"{{this.theme.instagram_url}}\" target=\"_blank\"><span class=\"iconify\" data-icon=\"ion-logo-instagram\" data-inline=\"false\"></span></a></li>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t{% if this.theme.vk_url %}
\t\t\t\t\t\t\t<li><a href=\"{{this.theme.vk_url}}\" target=\"_blank\"><span class=\"iconify\" data-icon=\"ion-logo-vk\" data-inline=\"false\"></span></a></li>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t{% if this.theme.pinterest_url %}
\t\t\t\t\t\t\t<li><a href=\"{{this.theme.pinterest_url}}\" target=\"_blank\"><span class=\"iconify\" data-icon=\"ion-logo-pinterest\" data-inline=\"false\"></span></a></li>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t{% if this.theme.mailru_url %}
\t\t\t\t\t\t\t<li><a href=\"{{this.theme.mailru_url}}\" target=\"_blank\"><span class=\"iconify\" data-icon=\"ion-at-circle-outline\" data-inline=\"false\"></span></a>
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t
\t\t\t\t\t</ul>

\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t\t\t<div class=\"footer-section\">
\t\t\t\t\t<h4 class=\"title\"><b>Категории</b></h4>
\t\t\t\t\t
\t\t\t\t\t{% for category in categories|batch(3) %}
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t{% for column in category %}
\t\t\t\t\t\t\t\t<li><a href=\"{{column.url}}\">{{column.name}}</a></li>
\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t</ul>
\t\t\t\t\t{% endfor %}
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t\t<div class=\"footer-section\">
        \t\t\t<div id=\"fb-root\"></div>
\t\t\t        <div id=\"user-info\"></div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"login\">
\t\t\t  \t<div class=\"modal-dialog\" role=\"document\">
\t\t\t    \t<div class=\"modal-content\">
\t\t\t      \t\t<div class=\"modal-header\">
\t\t\t        \t\t<h5 class=\"modal-title\">Войти с помощью</h5>
\t\t\t        \t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
\t\t\t          \t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t\t        \t\t</button>
\t\t\t      \t\t</div>
\t\t\t      \t\t<div class=\"modal-body\">
\t\t\t        \t\t<p style=\"text-align: center;\">
\t\t\t        \t\t\t<a href=\"#\" onClick=\"loginVk('{{'social'|page({ slug: 'vk' })}}'); return false;\"><img src=\"{{ 'social/vk.png'|media }}\"></a>&nbsp;
\t\t\t        \t\t\t<a href=\"#\" id=\"fb-auth\"><img src=\"{{ 'social/fb.png'|media }}\"></a>
\t\t\t        \t\t\t<a href=\"#\" id=\"g-auth\"><img src=\"{{ 'social/gp.png'|media }}\"></a>
\t\t\t        \t\t\t<a href=\"#\"  onClick=\"loginMailru('{{'social'|page({ slug: 'mailru' })}}'); return false;\"><img src=\"{{ 'social/mailru.png'|media }}\"></a>
\t\t\t        \t\t</p>
\t\t\t      \t\t</div>
\t\t\t    \t</div>
\t\t\t  \t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</footer>", "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/site/footer.htm", "");
    }
}
