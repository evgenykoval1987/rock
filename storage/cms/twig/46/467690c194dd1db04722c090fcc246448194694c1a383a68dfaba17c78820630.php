<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/plugins/saurabhdhariwal/comments/components/comments/form.htm */
class __TwigTemplate_cb5c5932ce1a6be6be4e89836c299fbeda00f9fd3e5043b0a0c9891e75934cd3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["user"] ?? null)) {
            // line 2
            echo "<div id=\"wrap-comment-form\">
    <div class=\"comment-form\" id=\"comment-form\">
        <div id=\"cancel-comment-reply-link\" style=\"display: none; text-align: right;\">
            <a onclick=\"Comment.cancel()\" rel=\"nofollow\" title=\"Отмена\" style=\"font-size: 26px\">×</a>
        </div>
        <form>
            <div id=\"comment_flash_message\"></div>
            <div class=\"row\">
                <div class=\"col-sm-6\">
                    <input type=\"text\" name=\"author\" class=\"form-control\" placeholder=\"Имя\" value=\"";
            // line 11
            ((($context["user"] ?? null)) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "name", [], "any", false, false, false, 11), "html", null, true))) : (print ("")));
            echo "\">
                </div>
                <div class=\"col-sm-6\">
                    <input type=\"email\" aria-required=\"true\" name=\"email\" class=\"form-control\"
                        placeholder=\"Email\" aria-invalid=\"true\"  value=\"";
            // line 15
            ((($context["user"] ?? null)) ? (print (twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "email", [], "any", false, false, false, 15), "html", null, true))) : (print ("")));
            echo "\">
                </div>
                <div class=\"col-sm-12\">
                    <textarea id=\"comment-text\" name=\"content\" rows=\"2\" class=\"text-area-messge form-control\"
                        placeholder=\"Текст\" aria-required=\"true\" aria-invalid=\"false\"></textarea >
                </div>
                <div class=\"col-sm-12\">
                    <button class=\"submit-btn\" type=\"submit\" id=\"form-submit\" onclick=\"Comment.saveButton(event)\"><b>Отправить</b></button>
                </div>

                <input name=\"post_id\" type=\"hidden\" value=\"";
            // line 25
            echo twig_escape_filter($this->env, ($context["post"] ?? null), "html", null, true);
            echo "\">
            </div>
        </form>
    </div>
</div>
";
        } else {
            // line 31
            echo "    <div id=\"wrap-comment-form\">
        <p style=\"text-align: center; padding: 30px 0\">Вы можете оставить свой комментарий. Для этого войдите в свою учетную запись.</p>
        <p style=\"text-align: center; font-size: 22px; margin-bottom: 20px\"><a class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#login\" href=\"#\">Войти</a></p>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/plugins/saurabhdhariwal/comments/components/comments/form.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 31,  70 => 25,  57 => 15,  50 => 11,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if user %}
<div id=\"wrap-comment-form\">
    <div class=\"comment-form\" id=\"comment-form\">
        <div id=\"cancel-comment-reply-link\" style=\"display: none; text-align: right;\">
            <a onclick=\"Comment.cancel()\" rel=\"nofollow\" title=\"Отмена\" style=\"font-size: 26px\">×</a>
        </div>
        <form>
            <div id=\"comment_flash_message\"></div>
            <div class=\"row\">
                <div class=\"col-sm-6\">
                    <input type=\"text\" name=\"author\" class=\"form-control\" placeholder=\"Имя\" value=\"{{ (user) ? user.name : '' }}\">
                </div>
                <div class=\"col-sm-6\">
                    <input type=\"email\" aria-required=\"true\" name=\"email\" class=\"form-control\"
                        placeholder=\"Email\" aria-invalid=\"true\"  value=\"{{ (user) ? user.email : '' }}\">
                </div>
                <div class=\"col-sm-12\">
                    <textarea id=\"comment-text\" name=\"content\" rows=\"2\" class=\"text-area-messge form-control\"
                        placeholder=\"Текст\" aria-required=\"true\" aria-invalid=\"false\"></textarea >
                </div>
                <div class=\"col-sm-12\">
                    <button class=\"submit-btn\" type=\"submit\" id=\"form-submit\" onclick=\"Comment.saveButton(event)\"><b>Отправить</b></button>
                </div>

                <input name=\"post_id\" type=\"hidden\" value=\"{{post}}\">
            </div>
        </form>
    </div>
</div>
{% else %}
    <div id=\"wrap-comment-form\">
        <p style=\"text-align: center; padding: 30px 0\">Вы можете оставить свой комментарий. Для этого войдите в свою учетную запись.</p>
        <p style=\"text-align: center; font-size: 22px; margin-bottom: 20px\"><a class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#login\" href=\"#\">Войти</a></p>
    </div>
{% endif %}", "C:\\local\\OpenServer\\domains\\kaktv/plugins/saurabhdhariwal/comments/components/comments/form.htm", "");
    }
}
