<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/themes/responsiv-clean/layouts/default.htm */
class __TwigTemplate_34d9d29e7b934122fa4a80cc788edc842b0286f4668a0a8dea24d9285f984aa2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 4
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/meta"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 5
        echo "    </head>
    <body>
        <!-- Header -->
        <header>
            ";
        // line 9
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 10
        echo "        </header>

        <!-- Sidebar -->
        <!--<section id=\"layout-sidebar\">
            ";
        // line 14
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/sidebar"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 15
        echo "        </section>-->

        <!-- Content -->
        <!--<section id=\"layout-content\">-->
            ";
        // line 19
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 20
        echo "        <!--</section>-->

        <!-- Footer -->
        <!--<footer id=\"layout-footer\">-->
            ";
        // line 24
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 25
        echo "        <!--</footer>-->
        
        <!-- Scripts -->
        ";
        // line 28
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/scripts"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 29
        echo "    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/layouts/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 29,  89 => 28,  84 => 25,  80 => 24,  74 => 20,  72 => 19,  66 => 15,  62 => 14,  56 => 10,  52 => 9,  46 => 5,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>
        {% partial 'site/meta' %}
    </head>
    <body>
        <!-- Header -->
        <header>
            {% partial 'site/header' %}
        </header>

        <!-- Sidebar -->
        <!--<section id=\"layout-sidebar\">
            {% partial 'site/sidebar' %}
        </section>-->

        <!-- Content -->
        <!--<section id=\"layout-content\">-->
            {% page %}
        <!--</section>-->

        <!-- Footer -->
        <!--<footer id=\"layout-footer\">-->
            {% partial 'site/footer' %}
        <!--</footer>-->
        
        <!-- Scripts -->
        {% partial 'site/scripts' %}
    </body>
</html>", "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/layouts/default.htm", "");
    }
}
