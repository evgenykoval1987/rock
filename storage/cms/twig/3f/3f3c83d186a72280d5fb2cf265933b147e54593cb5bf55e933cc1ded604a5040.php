<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/themes/responsiv-clean/partials/home/posts.htm */
class __TwigTemplate_067a73baaaaec04f07971a413860b69e0c8e5da2b0030385dc70034ba43ab5a0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 2
            echo "\t";
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 2), [0 => 1, 1 => 2, 2 => 3, 3 => 5, 4 => 7])) {
                // line 3
                echo "\t    <div class=\"col-lg-4 col-md-6\">
\t    \t<div class=\"card h-100\">
\t\t\t\t<div class=\"single-post post-style-1\">
\t        \t\t";
                // line 6
                $context['__cms_partial_params'] = [];
                $context['__cms_partial_params']['post'] = $context["post"]                ;
                echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/post"                , $context['__cms_partial_params']                , true                );
                unset($context['__cms_partial_params']);
                // line 7
                echo "\t        \t</div>
\t        </div>
\t    </div>
\t";
            } elseif (twig_in_filter(twig_get_attribute($this->env, $this->source,             // line 10
$context["loop"], "index", [], "any", false, false, false, 10), [0 => 4, 1 => 12])) {
                // line 11
                echo "\t\t<div class=\"col-lg-8 col-md-12\">
\t\t\t<div class=\"card h-100\">
\t\t\t\t<div class=\"single-post post-style-2\">
\t\t\t\t\t";
                // line 14
                $context['__cms_partial_params'] = [];
                $context['__cms_partial_params']['post'] = $context["post"]                ;
                echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/post-big"                , $context['__cms_partial_params']                , true                );
                unset($context['__cms_partial_params']);
                // line 15
                echo "\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t";
            } elseif (twig_in_filter(twig_get_attribute($this->env, $this->source,             // line 18
$context["loop"], "index", [], "any", false, false, false, 18), [0 => 6])) {
                // line 19
                echo "\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t<div class=\"card h-100\">
\t\t\t\t<div class=\"single-post post-style-2 post-style-3\">
\t\t\t\t\t";
                // line 22
                $context['__cms_partial_params'] = [];
                $context['__cms_partial_params']['post'] = $context["post"]                ;
                echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/post-without-image"                , $context['__cms_partial_params']                , true                );
                unset($context['__cms_partial_params']);
                // line 23
                echo "\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t";
            } elseif (twig_in_filter(twig_get_attribute($this->env, $this->source,             // line 26
$context["loop"], "index", [], "any", false, false, false, 26), [0 => 8])) {
                // line 27
                echo "\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t<div class=\"card h-100\">
\t\t\t\t";
                // line 29
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
                    // line 30
                    echo "\t\t\t\t\t";
                    if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 30), [0 => 8])) {
                        // line 31
                        echo "\t\t\t\t\t\t<div class=\"single-post post-style-4\">
\t\t\t\t\t\t\t";
                        // line 32
                        $context['__cms_partial_params'] = [];
                        $context['__cms_partial_params']['post'] = $context["post"]                        ;
                        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/post-short"                        , $context['__cms_partial_params']                        , true                        );
                        unset($context['__cms_partial_params']);
                        // line 33
                        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t";
                    } elseif (twig_in_filter(twig_get_attribute($this->env, $this->source,                     // line 34
$context["loop"], "index", [], "any", false, false, false, 34), [0 => 9])) {
                        // line 35
                        echo "\t\t\t\t\t\t<div class=\"single-post\">
\t\t\t\t\t\t\t";
                        // line 36
                        $context['__cms_partial_params'] = [];
                        $context['__cms_partial_params']['post'] = $context["post"]                        ;
                        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/post-short"                        , $context['__cms_partial_params']                        , true                        );
                        unset($context['__cms_partial_params']);
                        // line 37
                        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t";
                    }
                    // line 39
                    echo "\t\t\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 40
                echo "\t\t\t</div>
\t\t</div>
\t";
            } elseif (twig_in_filter(twig_get_attribute($this->env, $this->source,             // line 42
$context["loop"], "index", [], "any", false, false, false, 42), [0 => 10])) {
                // line 43
                echo "\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t<div class=\"card h-100\">
\t\t\t\t";
                // line 45
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
                    // line 46
                    echo "\t\t\t\t\t";
                    if (twig_in_filter(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 46), [0 => 10])) {
                        // line 47
                        echo "\t\t\t\t\t\t<div class=\"single-post post-style-4\">
\t\t\t\t\t\t\t";
                        // line 48
                        $context['__cms_partial_params'] = [];
                        $context['__cms_partial_params']['post'] = $context["post"]                        ;
                        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/post-short"                        , $context['__cms_partial_params']                        , true                        );
                        unset($context['__cms_partial_params']);
                        // line 49
                        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t";
                    } elseif (twig_in_filter(twig_get_attribute($this->env, $this->source,                     // line 50
$context["loop"], "index", [], "any", false, false, false, 50), [0 => 11])) {
                        // line 51
                        echo "\t\t\t\t\t\t<div class=\"single-post\">
\t\t\t\t\t\t\t";
                        // line 52
                        $context['__cms_partial_params'] = [];
                        $context['__cms_partial_params']['post'] = $context["post"]                        ;
                        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/post-short"                        , $context['__cms_partial_params']                        , true                        );
                        unset($context['__cms_partial_params']);
                        // line 53
                        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t";
                    }
                    // line 55
                    echo "\t\t\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 56
                echo "\t\t\t</div>
\t\t</div>
\t";
            }
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/home/posts.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  240 => 56,  226 => 55,  222 => 53,  217 => 52,  214 => 51,  212 => 50,  209 => 49,  204 => 48,  201 => 47,  198 => 46,  181 => 45,  177 => 43,  175 => 42,  171 => 40,  157 => 39,  153 => 37,  148 => 36,  145 => 35,  143 => 34,  140 => 33,  135 => 32,  132 => 31,  129 => 30,  112 => 29,  108 => 27,  106 => 26,  101 => 23,  96 => 22,  91 => 19,  89 => 18,  84 => 15,  79 => 14,  74 => 11,  72 => 10,  67 => 7,  62 => 6,  57 => 3,  54 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% for post in posts %}
\t{% if loop.index in [1,2,3,5,7] %}
\t    <div class=\"col-lg-4 col-md-6\">
\t    \t<div class=\"card h-100\">
\t\t\t\t<div class=\"single-post post-style-1\">
\t        \t\t{% partial 'blog/post' post=post %}
\t        \t</div>
\t        </div>
\t    </div>
\t{% elseif loop.index in [4,12] %}
\t\t<div class=\"col-lg-8 col-md-12\">
\t\t\t<div class=\"card h-100\">
\t\t\t\t<div class=\"single-post post-style-2\">
\t\t\t\t\t{% partial 'blog/post-big' post=post %}
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t{% elseif loop.index in [6] %}
\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t<div class=\"card h-100\">
\t\t\t\t<div class=\"single-post post-style-2 post-style-3\">
\t\t\t\t\t{% partial 'blog/post-without-image' post=post %}
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t{% elseif loop.index in [8] %}
\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t<div class=\"card h-100\">
\t\t\t\t{% for post in posts %}
\t\t\t\t\t{% if loop.index in [8] %}
\t\t\t\t\t\t<div class=\"single-post post-style-4\">
\t\t\t\t\t\t\t{% partial 'blog/post-short' post=post %}
\t\t\t\t\t\t</div>
\t\t\t\t\t{% elseif loop.index in [9] %}
\t\t\t\t\t\t<div class=\"single-post\">
\t\t\t\t\t\t\t{% partial 'blog/post-short' post=post %}
\t\t\t\t\t\t</div>
\t\t\t\t\t{% endif %}
\t\t\t\t{% endfor %}
\t\t\t</div>
\t\t</div>
\t{% elseif loop.index in [10] %}
\t\t<div class=\"col-lg-4 col-md-6\">
\t\t\t<div class=\"card h-100\">
\t\t\t\t{% for post in posts %}
\t\t\t\t\t{% if loop.index in [10] %}
\t\t\t\t\t\t<div class=\"single-post post-style-4\">
\t\t\t\t\t\t\t{% partial 'blog/post-short' post=post %}
\t\t\t\t\t\t</div>
\t\t\t\t\t{% elseif loop.index in [11] %}
\t\t\t\t\t\t<div class=\"single-post\">
\t\t\t\t\t\t\t{% partial 'blog/post-short' post=post %}
\t\t\t\t\t\t</div>
\t\t\t\t\t{% endif %}
\t\t\t\t{% endfor %}
\t\t\t</div>
\t\t</div>
\t{% endif %}
{% endfor %}", "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/home/posts.htm", "");
    }
}
