<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/themes/responsiv-clean/pages/blog/tag.htm */
class __TwigTemplate_90dc3831249c83d87316100df307db89f8bba17b1a560aa31064f3f57aa83188 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"slider display-table center-text\">
\t<h1 class=\"title display-table-cell\"><b>";
        // line 2
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogTagSearch"] ?? null), "tag", [], "any", false, false, false, 2), "name", [], "any", false, false, false, 2), "html", null, true);
        echo "</b></h1>
</div>
<section class=\"blog-area section\">
\t<div class=\"container\">
\t\t<div class=\"row\" id=\"category-posts\">
\t\t\t";
        // line 7
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/posts"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 8
        echo "\t\t</div>
\t\t
\t\t";
        // line 10
        if ((twig_get_attribute($this->env, $this->source, ($context["blogTagSearch"] ?? null), "lastPage", [], "any", false, false, false, 10) != twig_get_attribute($this->env, $this->source, ($context["blogTagSearch"] ?? null), "currentPage", [], "any", false, false, false, 10))) {
            // line 11
            echo "\t\t<a class=\"load-more-btn\" href=\"#\" 
                data-attach-loading 
                data-control=\"auto-pager\"
                data-request=\"onPagePosts\"
                data-request-update=\"'blog/posts': '@#category-posts'\"
                data-current-page=\"1\"
                data-last-page=\"";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["blogTagSearch"] ?? null), "lastPage", [], "any", false, false, false, 17), "html", null, true);
            echo "\"><b>ПОКАЗАТЬ ЕЩЕ</b></a>
        ";
        }
        // line 19
        echo "\t</div>
</section>";
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/pages/blog/tag.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 19,  66 => 17,  58 => 11,  56 => 10,  52 => 8,  48 => 7,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"slider display-table center-text\">
\t<h1 class=\"title display-table-cell\"><b>{{ blogTagSearch.tag.name }}</b></h1>
</div>
<section class=\"blog-area section\">
\t<div class=\"container\">
\t\t<div class=\"row\" id=\"category-posts\">
\t\t\t{% partial 'blog/posts' %}
\t\t</div>
\t\t
\t\t{% if blogTagSearch.lastPage != blogTagSearch.currentPage%}
\t\t<a class=\"load-more-btn\" href=\"#\" 
                data-attach-loading 
                data-control=\"auto-pager\"
                data-request=\"onPagePosts\"
                data-request-update=\"'blog/posts': '@#category-posts'\"
                data-current-page=\"1\"
                data-last-page=\"{{ blogTagSearch.lastPage }}\"><b>ПОКАЗАТЬ ЕЩЕ</b></a>
        {% endif %}
\t</div>
</section>", "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/pages/blog/tag.htm", "");
    }
}
