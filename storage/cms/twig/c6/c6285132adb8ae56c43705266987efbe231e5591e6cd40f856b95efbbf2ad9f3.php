<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\local\OpenServer\domains\kaktv/themes/responsiv-clean/partials/site/header.htm */
class __TwigTemplate_33be4baa979a6c138bafcf5e64f630a6f0bde2a26aa956d45717d11ec7c2b656 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"container-fluid position-relative no-side-padding\">
    <a href=\"";
        // line 2
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "\" class=\"logo\"><img src=\"";
        echo $this->extensions['System\Twig\Extension']->mediaFilter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "theme", [], "any", false, false, false, 2), "logo", [], "any", false, false, false, 2));
        echo "\" alt=\"Logo Image\"></a>

    <div class=\"menu-nav-icon\" data-nav-menu=\"#main-menu\"><i class=\"ion-navicon\"></i></div>

    <ul class=\"main-menu visible-on-click\" id=\"main-menu\">
        <li><a href=\"";
        // line 7
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "\">Главная</a></li>
        <li><a href=\"";
        // line 8
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blog/categories");
        echo "\">Категории</a></li>
        <li><a href=\"";
        // line 9
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("discussed");
        echo "\">Обсуждаемое</a></li>
        ";
        // line 10
        if (($context["user"] ?? null)) {
            // line 11
            echo "            <li class=\"user-block-mobile\">
                <div class=\"user-account-logo\">
                    <img src=\"";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "getAvatarThumb", [0 => "300"], "method", false, false, false, 13), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "name", [], "any", false, false, false, 13), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "surname", [], "any", false, false, false, 13), "html", null, true);
            echo "\">
                </div>
                <div class=\"user-account-info\">
                    ";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "name", [], "any", false, false, false, 16), "html", null, true);
            echo " 
                </div>
            </li>
            <li class=\"user-block-mobile-login\"><a data-request=\"session::onLogout\" data-request-data=\"redirect: '/'\" href=\"#\">Выйти</a></li>
        ";
        } else {
            // line 21
            echo "            <li class=\"user-block-mobile-login\"><a class=\"user-login-button\" data-toggle=\"modal\" data-target=\"#login\" href=\"#\">Войти</a></li>
        ";
        }
        // line 23
        echo "    </ul>

    
    <div class=\"user-block\">
        ";
        // line 27
        if (($context["user"] ?? null)) {
            // line 28
            echo "            <div class=\"user-account\">
                <div class=\"user-account-logo\">
                    <img src=\"";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "getAvatarThumb", [0 => "300"], "method", false, false, false, 30), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "name", [], "any", false, false, false, 30), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "surname", [], "any", false, false, false, 30), "html", null, true);
            echo "\">
                </div>
                <div class=\"user-account-info\">
                    ";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "name", [], "any", false, false, false, 33), "html", null, true);
            echo "&nbsp;|&nbsp;<a data-request=\"session::onLogout\" data-request-data=\"redirect: '/'\" href=\"#\">Выйти</a>
                </div>
            </div>
        ";
        } else {
            // line 37
            echo "            <a class=\"user-login-button\" data-toggle=\"modal\" data-target=\"#login\" href=\"#\">Войти</a>
        ";
        }
        // line 39
        echo "    </div>

    <div class=\"src-area\">
        <form action=\"";
        // line 42
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("search");
        echo "\" method=\"get\">
            <button class=\"src-btn\" type=\"submit\"><i class=\"ion-ios-search-strong\"></i></button>
            <input class=\"src-input\" type=\"text\" placeholder=\"Поиск\" name=\"q\" autocomplete=\"off\">
        </form>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/site/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 42,  123 => 39,  119 => 37,  112 => 33,  102 => 30,  98 => 28,  96 => 27,  90 => 23,  86 => 21,  78 => 16,  68 => 13,  64 => 11,  62 => 10,  58 => 9,  54 => 8,  50 => 7,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"container-fluid position-relative no-side-padding\">
    <a href=\"{{ 'home'|page }}\" class=\"logo\"><img src=\"{{ this.theme.logo|media }}\" alt=\"Logo Image\"></a>

    <div class=\"menu-nav-icon\" data-nav-menu=\"#main-menu\"><i class=\"ion-navicon\"></i></div>

    <ul class=\"main-menu visible-on-click\" id=\"main-menu\">
        <li><a href=\"{{ 'home'|page }}\">Главная</a></li>
        <li><a href=\"{{ 'blog/categories'|page }}\">Категории</a></li>
        <li><a href=\"{{ 'discussed'|page }}\">Обсуждаемое</a></li>
        {% if user %}
            <li class=\"user-block-mobile\">
                <div class=\"user-account-logo\">
                    <img src=\"{{user.getAvatarThumb('300')}}\" title=\"{{ user.name }} {{ user.surname }}\">
                </div>
                <div class=\"user-account-info\">
                    {{ user.name }} 
                </div>
            </li>
            <li class=\"user-block-mobile-login\"><a data-request=\"session::onLogout\" data-request-data=\"redirect: '/'\" href=\"#\">Выйти</a></li>
        {% else %}
            <li class=\"user-block-mobile-login\"><a class=\"user-login-button\" data-toggle=\"modal\" data-target=\"#login\" href=\"#\">Войти</a></li>
        {% endif %}
    </ul>

    
    <div class=\"user-block\">
        {% if user %}
            <div class=\"user-account\">
                <div class=\"user-account-logo\">
                    <img src=\"{{user.getAvatarThumb('300')}}\" title=\"{{ user.name }} {{ user.surname }}\">
                </div>
                <div class=\"user-account-info\">
                    {{ user.name }}&nbsp;|&nbsp;<a data-request=\"session::onLogout\" data-request-data=\"redirect: '/'\" href=\"#\">Выйти</a>
                </div>
            </div>
        {% else %}
            <a class=\"user-login-button\" data-toggle=\"modal\" data-target=\"#login\" href=\"#\">Войти</a>
        {% endif %}
    </div>

    <div class=\"src-area\">
        <form action=\"{{ 'search' | page }}\" method=\"get\">
            <button class=\"src-btn\" type=\"submit\"><i class=\"ion-ios-search-strong\"></i></button>
            <input class=\"src-input\" type=\"text\" placeholder=\"Поиск\" name=\"q\" autocomplete=\"off\">
        </form>
    </div>
</div>", "C:\\local\\OpenServer\\domains\\kaktv/themes/responsiv-clean/partials/site/header.htm", "");
    }
}
