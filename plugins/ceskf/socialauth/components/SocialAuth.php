<?php 

namespace Ceskf\SocialAuth\Components;

use Cms\Classes\ComponentBase;
use DB;
use Auth;
use RainLab\User\Models\User as AuthUser;
use RainLab\User\Components\Account;
use Redirect;

class SocialAuth extends ComponentBase
{
    
    public function componentDetails()
    {
        return [
            'name'        => 'Social Auth',
            'description' => 'Plugin for social auth.'
        ];
    }

    public function onRun(){
        $social = $this->param('slug');
        if($social == 'vk'){
            $this->vk(get('code'));
            return  Redirect::to('/');
        }
        if($social == 'fb'){
            $this->fb();
            return 1;
        }
        if($social == 'gp'){
            $this->gp();
            return 1;
        }
        if($social == 'mailru'){
            $this->mailru(get('code'));
            return '';
        }
    }

    protected function vk($code){
        if ($code) {
            $params = array(
                'client_id' => 7209627,
                'client_secret' => 'sy5c866fhWsb6E2RHn3m',
                'code' => $code,
                'redirect_uri' => $this->pageUrl('social','vk'),
                'v'=>'5.92'
            );

            $token = json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($params))), true);
            
            $email = $token['email'];

            if (isset($token['access_token'])) {
                $params = array(
                    'uids'         => $token['user_id'],
                    'fields'       => 'uid,first_name,last_name,screen_name,sex,bdate,photo_big,email',
                    'access_token' => $token['access_token'],
                    'v'=>'5.92'
                );

                $userInfo = json_decode(file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($params))), true);

                $userInfo = $userInfo['response'];

                $first_name = $userInfo[0]['first_name'];
                $last_name = $userInfo[0]['last_name'];
                $photo = $userInfo[0]['photo_big'];
                
                if (AuthUser::findByEmail($email)){
                    $account = new Account();
                    $account->onSignin(array('login' => $email, 'password' => '11111111', 'email' => $email));
                }
                else{
                    $account = new Account();
                    $info = [
                        'username' => $email, 
                        'password' => '11111111', 
                        'password_confirmation' => '11111111',
                        'email' => $email,
                        'name' => $first_name,
                        'surname' => $last_name,
                        'photo' => str_replace('?ava=1', '', $photo)
                    ];
                    $account->onRegister($info);
                    
                    
                }
                //dd($userInfo);
            }
        }

    }

    protected function fb(){
        $email = post('email');
        $first_name = post('first_name');
        $last_name = post('last_name');
        $photo = post('image');
        
        if (AuthUser::findByEmail($email)){
            $account = new Account();
            $account->onSignin(array('login' => $email, 'password' => '11111111', 'email' => $email));
        }
        else{
            $account = new Account();
            $info = [
                'username' => $email, 
                'password' => '11111111', 
                'password_confirmation' => '11111111',
                'email' => $email,
                'name' => $first_name,
                'surname' => $last_name,
                'photo' => str_replace('?ava=1', '', $photo)
            ];
            $account->onRegister($info);
        }   
    }

    protected function gp(){
        $email = post('email');
        $first_name = post('first_name');
        $last_name = post('last_name');
        $photo = post('photo');
        
        if (AuthUser::findByEmail($email)){
            $account = new Account();
            $account->onSignin(array('login' => $email, 'password' => '11111111', 'email' => $email));
        }
        else{
            $account = new Account();
            $info = [
                'username' => $email, 
                'password' => '11111111', 
                'password_confirmation' => '11111111',
                'email' => $email,
                'name' => $first_name,
                'surname' => $last_name,
                'photo' => str_replace('?ava=1', '', $photo)
            ];
            $account->onRegister($info);
        }   
    }

    protected function mailru($code){
        if ($code) {
            $ch = curl_init();
            $url = "https://connect.mail.ru/oauth/token";
            $fields = Array(
                'client_id'     =>  '768132',
                'client_secret'     =>  'e98167119d9583854ba1d25d5c42495d',
                'grant_type'        =>  "authorization_code",
                'code'          =>  $code,
                'redirect_uri'      =>  urlencode('https://kaktv.top/social/mailru')
            );
            $fields_string = '';
            foreach($fields as $key => $value){
                $fields_string .= $key . '=' . $value . '&';
            }
            rtrim($fields_string, '&');

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

            $result = curl_exec($ch);
            curl_close($ch);
                    
            $arr = json_decode($result, true);

            $token  = $arr['access_token'];
            $uid    = $arr['x_mailru_vid'];
            
            $request_params = Array(
                'app_id'    =>  '768132',
                'uids'      =>  $uid,
                'method'    =>  'users.getInfo',
                'secure'    =>  1,
                'session_key'   =>  $token
            );
            ksort($request_params);
            $params = '';
            foreach ($request_params as $key => $value) {
                $params .= "$key=$value";
            }
            $sig = md5($params . 'e98167119d9583854ba1d25d5c42495d');

            //Формируем запрос
            $url = "http://www.appsmail.ru/platform/api?method=users.getInfo&app_id=768132&session_key={$token}&sig={$sig}&uids={$uid}&secure=1";

            $response = file_get_contents($url);

            $info = (array) json_decode($response);
            $info = $info[0];

            $email = $info->email;
            $first_name = $info->first_name;
            $last_name = $info->last_name;
            $photo = $info->pic;
            
            if (AuthUser::findByEmail($email)){
                $account = new Account();
                $account->onSignin(array('login' => $email, 'password' => '11111111', 'email' => $email));
            }
            else{
                $account = new Account();
                $info = [
                    'username' => $email, 
                    'password' => '11111111', 
                    'password_confirmation' => '11111111',
                    'email' => $email,
                    'name' => $first_name,
                    'surname' => $last_name,
                    'photo' => str_replace('?ava=1', '', $photo)
                ];
                $account->onRegister($info);
            }
        }

    }
}