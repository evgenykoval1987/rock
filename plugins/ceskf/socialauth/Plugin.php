<?php

namespace Ceskf\SocialAuth;

use System\Classes\PluginBase;

class Plugin extends PluginBase {

    public function pluginDetails() {
        return [
            'name'        => 'Social Auth',
            'description' => 'Social Auth',
            'author'      => 'Ceskf',
            'icon'        => 'icon-share-alt'
        ];
    }

    public function registerComponents() {
        return [
            'Ceskf\SocialAuth\Components\SocialAuth'    => 'socialauth'
        ];
    }

}

?>